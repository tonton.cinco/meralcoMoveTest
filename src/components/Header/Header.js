import React, {Component} from 'react';
import {
  Badge,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  NavbarToggler,
  NavbarBrand,
  DropdownToggle
} from 'reactstrap';

class Header extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  render() {
    return (
      <header className="app-header navbar navbar-fixed-top topheader">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>&#9776;</NavbarToggler>
        <NavbarBrand href="#"></NavbarBrand>
        <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}>&#9776;</NavbarToggler>
        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink href="#">CMS</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink href="#">Reports</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink href="#">Management</NavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>        
          <NavItem>
            <img src={'img/avatars/avatar.png'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
            <span> Hi, admin</span>
          </NavItem>
          <NavItem className="px-3">
            <NavLink href="#">Logout</NavLink>
          </NavItem>
        </Nav>
      </header>
    )
  }
}

export default Header;
