export default {
  items: [
    {
      name: 'Directory',
      url: '/directory/list'
    },
    {
      name: 'Bright Ideas',
      url: '/brightideas/list',
    },
    {
      name: 'Orange Tag',
      url: '/orangetag',
    },
    {
      name: 'App Cal',
      url: '/appcal',
      children: [
        {
          name: 'App Cal Rates',
          url: '/appcal/appcalrates',          
        },
        {
          name: 'App Cal Appliance',
          url: '/appcal/appcalappliance',
        }
      ]
    },
    {
      name: 'Pages',
      url: '/pages',
      icon: 'icon-star',
      children: [
        {
          name: 'Login',
          url: '/login',
          icon: 'icon-star'
        },
        {
          name: 'Register',
          url: '/register',
          icon: 'icon-star'
        },
        {
          name: 'Error 404',
          url: '/404',
          icon: 'icon-star'
        },
        {
          name: 'Error 500',
          url: '/500',
          icon: 'icon-star'
        }
      ]
    }
  ]
};
