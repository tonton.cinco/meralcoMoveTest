import rows from './demo-data-rows.js';

/**
 * customOptions and customFilter are used for #demo 2
 * they serve as an example of how to customize the options and labels in the datatable
 */

 /* eslint-disable */

const customFilter = (rows, columns, searchQuery = '') => {
  // custom logic filter –> looks for match of the searchQuery in the name field only
  return rows.filter(row => row.name.toLowerCase().indexOf(searchQuery.toLowerCase()) > -1);
}

const customOptions = {
  itemsPerPageOptions: false,
  itemsPerPageLabel: '',
  nextPageLabel: 'next',
  previousPageLabel: 'previous',
  searchLabel: ' ',
  searchPlaceholder: 'Search',
  noEntriesLabel: 'No record found.',
  entryCountLabels: ['Showing: ', 'to', 'of', 'records.'],
  showDownloadCSVButton: false,
  downloadCSVButtonLabel: '',
  customFilter
};

const columns = [{
    key: 'id',
    label: '#',
  }, {
    key: 'name',
    label: 'Name',
  }, {
    key: 'email',
    label: 'Email',
  }, {
    key: 'state',
    label: 'State',
  },
];

export default {
  columns,
  rows,
  customOptions,
};
