import React, {Component} from "react";
import {NavbarBrand, Container, Row, Col, CardGroup, Card, CardBlock, Button, Input, InputGroup, InputGroupAddon} from "reactstrap";

class Login extends Component {
  render() {
    return (
      <div>
      <header className="app-header navbar topheader">
      <NavbarBrand href="#"></NavbarBrand>
      </header>
      <div className="app app-login flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  <CardBlock className="card-body">                   
                    <InputGroup className="mb-3">
                      <InputGroupAddon><i className="icon-user"></i></InputGroupAddon>
                      <Input size="25" type="text" placeholder="MAN No."/>
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                      <Input type="password" placeholder="Password"/>
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                      <a href="#/directory/list" className="btn-meco-white">Login</a>                          
                      </Col>               
                    </Row>
                  </CardBlock>
                </Card>                
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
      </div>
    );
  }
}

export default Login;
