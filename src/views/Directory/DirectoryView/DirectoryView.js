import React, {Component} from "react";

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class DirectoryView extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Row xs="12">
          <Col>
            <Card>
              <CardBlock className="card-body">
                <FormGroup>
                  <Col xs="4">
                  <Label htmlFor="branch">Branch</Label>
                  <Input type="text" id="branch" placeholder=""/>
                  </Col>
                </FormGroup>
                <FormGroup>
                <Col xs="6">
                  <Label htmlFor="address">Address</Label>
                  <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                             placeholder=""/>
                </Col>
                </FormGroup>                
                <FormGroup row>
                <Col xs="4">
                  <Label htmlFor="area">Area</Label>
                  <Input type="text" id="area" placeholder=""/>
                </Col>
                <Col xs="4">                   
                      <Label htmlFor="longitude">Longtitude</Label>
                      <Input type="text" id="longitude" placeholder=""/>                    
                  </Col>
                  <Col xs="4">                    
                      <Label htmlFor="latitude">Latitude</Label>
                      <Input type="text" id="latitude" placeholder=""/>                    
                  </Col>
                </FormGroup>          
                <FormGroup row>
                <Col xs="4">
                  <Label htmlFor="contact">Contact</Label>
                  <Input type="text" id="contact" placeholder=""/>
                </Col>
                <Col xs="4">
                <Label htmlFor="type">Type</Label>
                  <Input type="select" name="select" id="select" size="sm">
                        <option value="0">Please select</option>
                        <option value="1">Business Center</option>
                        <option value="2">Bayad Center</option>                        
                  </Input>
                </Col>
                </FormGroup>                
                <FormGroup>
                  <Col xs="4">
                  <Label htmlFor="status">Status</Label>
                  <Input type="select" name="status" id="status" size="sm">
                        <option value="1" selected="selected">Active</option>
                        <option value="0">Inactive</option>                      
                  </Input>
                  </Col>
                </FormGroup> 
                <div className="btn-toolbar float-right">               
                  <Button type="submit"  className="btn-meco-white">Create</Button>
                  <Button type="reset" className="btn-meco-orange"> Cancel </Button>
                </div>
              </CardBlock>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default DirectoryView;
