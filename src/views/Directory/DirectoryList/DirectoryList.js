import React, {Component} from "react";
import SpicyDatatable from 'spicy-datatable';
import data from '../../Sample/demo-data.js';
const { columns, rows, customOptions } = data;

class DirectoryList extends Component {

  render() {
    return (
      <div className="animated fadeIn">              
        <div className="tbl-meco-list">
        <a href="#/directory/create" className="btn-meco-white">Create New Item</a>  
          <SpicyDatatable
            tableKey="demo-table-custom-options" // see below for prop documentation
            columns={columns}
            rows={rows}
            config={customOptions} // optional, used to override chosen default settings/labels
          />
        </div>
      </div>
    )
  }
}

export default DirectoryList;
