import React, {Component} from "react";
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class BrightideasCreate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      startDate: moment(),
      endDate: moment(),
      file: '',
      imagePreviewUrl: '../img/img_placeholder.png'
    };    
    this.handleChangeStart = this.handleChangeStart.bind(this);
    this.handleChangeEnd = this.handleChangeEnd.bind(this);
    this._handleImageChange = this._handleImageChange.bind(this);

  }
    handleChangeStart(date) {
      this.setState({
        startDate: date
      });
    }

    handleChangeEnd(date) {
      this.setState({
        endDate: date
      });
    }

    _handleImageChange(e) {
      e.preventDefault();
  
      let reader = new FileReader();
      let file = e.target.files[0];
  
      reader.onloadend = () => {
        this.setState({
          file: file,
          imagePreviewUrl: reader.result
        });
      }
  
      reader.readAsDataURL(file)
  }

  render() {

    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img src={imagePreviewUrl} />);

    }

    return (
      <div className="animated fadeIn">
        <Row xs="12">
          <Col>
            <Card>
              <CardBlock className="card-body">
                <FormGroup row>                
                <div className="upload-preview">
                {$imagePreview}
                </div>             
                <div className="upload-btn-wrapper">
                  <Label htmlFor="image">Upload Image</Label>
                  <Button className="btn btn-meco-white" name="image" id="image"> Browse File</Button>
                  <Input type="file"onChange={this._handleImageChange}/>
                </div>                
                </FormGroup>
                <FormGroup>
                  <Col xs="4">
                  <Label htmlFor="category">Category</Label>
                  <Input type="select" name="category" id="category" size="sm">
                        <option value="0">Please select</option>
                        <option value="1">Option #1</option>
                        <option value="2">Option #2</option>
                        <option value="3">Option #3</option>
                        <option value="4">Option #4</option>
                        <option value="5">Option #5</option>
                  </Input>
                  </Col>
                </FormGroup>             
                <FormGroup row>
                <Col xs="4">
                  <Label htmlFor="start">Promo Start Date</Label>
                  <DatePicker className="form-control" selected={this.state.startDate}
                    selectsStart
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    onChange={this.handleChangeStart}/>                  
                </Col>
                <Col xs="4">                   
                  <Label htmlFor="end">Promo End Date</Label>
                  <DatePicker className="form-control" selected={this.state.endDate}
                    selectsEnd
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    onChange={this.handleChangeEnd}/>               
                  </Col>     
                </FormGroup>
                <FormGroup>
                <Col xs="6">
                  <Label htmlFor="description">Description</Label>
                  <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                             placeholder=""/>
                </Col>
                </FormGroup>
                <FormGroup>
                <Col xs="6">
                  <Label htmlFor="details">Details</Label>
                  <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                             placeholder=""/>
                </Col>
                </FormGroup>                                             
                <FormGroup>
                <Col xs="4">
                  <Label htmlFor="status">Status</Label>
                  <Input type="select" name="status" id="status" size="sm">
                        <option value="1" defaultValue>Active</option>
                        <option value="0">Inactive</option>                      
                  </Input>
                  </Col>
                </FormGroup>                
                <div className="btn-toolbar float-right">               
                  <Button type="submit"  className="btn-meco-white">Create</Button>
                  <Button type="reset" className="btn-meco-orange"> Cancel </Button>
                </div>
              </CardBlock>
            </Card>
          </Col>
        </Row>

      </div>
    )
  }
}

export default BrightideasCreate;
