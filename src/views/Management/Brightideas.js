import React, {Component} from "react";
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class Forms extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row xs="12">
          <Col>
            <Card>
              <CardBlock className="card-body">
                <FormGroup>
                  <Col xs="4">
                  <Label htmlFor="company">Branch</Label>
                  <Input type="text" id="company" placeholder=""/>
                  </Col>
                </FormGroup>
                <FormGroup>
                <Col xs="6">
                  <Label htmlFor="vat">Address</Label>
                  <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                             placeholder="Content..."/>
                </Col>
                </FormGroup>                
                <FormGroup row>
                <Col xs="4">
                  <Label htmlFor="street">Area</Label>
                  <Input type="text" id="street" placeholder="Enter street name"/>
                </Col>
                <Col xs="4">                   
                      <Label htmlFor="city">Longtitude</Label>
                      <Input type="text" id="city" placeholder="Enter your city"/>                    
                  </Col>
                  <Col xs="4">                    
                      <Label htmlFor="postal-code">Latitude</Label>
                      <Input type="text" id="postal-code" placeholder="Postal Code"/>                    
                  </Col>
                </FormGroup>          
                <FormGroup row>
                <Col xs="4">
                  <Label htmlFor="country">Contact</Label>
                  <Input type="text" id="country" placeholder="Country name"/>
                </Col>
                <Col xs="4">
                <Label htmlFor="country">Type</Label>
                  <Input type="select" name="selectSm" id="SelectLm" size="sm">
                        <option value="0">Please select</option>
                        <option value="1">Option #1</option>
                        <option value="2">Option #2</option>
                        <option value="3">Option #3</option>
                        <option value="4">Option #4</option>
                        <option value="5">Option #5</option>
                  </Input>
                </Col>
                </FormGroup>                
                <FormGroup>
                  <Col xs="4">
                  <Label htmlFor="country">Status</Label>
                  <Input type="select" name="selectSm" id="SelectLm" size="sm">
                        <option value="0">Please select</option>
                        <option value="1">Option #1</option>
                        <option value="2">Option #2</option>
                        <option value="3">Option #3</option>
                        <option value="4">Option #4</option>
                        <option value="5">Option #5</option>
                  </Input>
                  </Col>
                </FormGroup>                
                <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
              </CardBlock>
            </Card>
          </Col>
        </Row>

      </div>
    )
  }
}

export default Forms;
