import React, {Component} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Footer from '../../components/Footer/';
import DirectoryList from '../../views/Directory/DirectoryList/';
import DirectoryCreate from '../../views/Directory/DirectoryCreate/';
import DirectoryView from '../../views/Directory/DirectoryView/';
import DirectoryModify from '../../views/Directory/DirectoryModify/';
import BrightideasList from '../../views/Brightideas/BrightideasList/';
import BrightideasCreate from '../../views/Brightideas/BrightideasCreate/';
import BrightideasView from '../../views/Brightideas/BrightideasView/';
import BrightideasModify from '../../views/Brightideas/BrightideasModify/';
import Orangetag from '../../views/Orangetag/';
import Appcalrates from '../../views/Appcal/Appcalrates/';
import Appcalappliance from '../../views/Appcal/Appcalappliance/';
import Charts from '../../views/Charts/';
import Widgets from '../../views/Widgets/';
import Buttons from '../../views/Components/Buttons/';
import Cards from '../../views/Components/Cards/';
import Forms from '../../views/Components/Forms/';
import Modals from '../../views/Components/Modals/';
import SocialButtons from '../../views/Components/SocialButtons/';
import Switches from '../../views/Components/Switches/';
import Tables from '../../views/Components/Tables/';
import Tabs from '../../views/Components/Tabs/';
import FontAwesome from '../../views/Icons/FontAwesome/';
import SimpleLineIcons from '../../views/Icons/SimpleLineIcons/';

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              <Switch>          
                <Route path="/directory/list" name="Directory" component={DirectoryList}/>
                <Route path="/directory/create" name="Directory Create" component={DirectoryCreate}/>
                <Route path="/directory/view" name="Directory View" component={DirectoryView}/>
                <Route path="/directory/update" name="Directory Update" component={DirectoryModify}/>
                <Route path="/brightideas/list" name="Brightideas" component={BrightideasList}/>
                <Route path="/brightideas/create" name="Brightideas Create" component={BrightideasCreate}/>
                <Route path="/brightideas/view" name="Brightideas View" component={BrightideasView}/>
                <Route path="/brightideas/update" name="Brightideas Update" component={BrightideasModify}/>
                <Route path="/orangetag" name="Orangetag" component={Orangetag}/>
                {/* <Route path="/appcal" name="Appcal" component={Appcal}/> */}
                <Route path="/appcal/appcalrates" name="Appcalrates" component={Appcalrates}/>
                <Route path="/appcal/appcalappliance" name="Appcalappliance" component={Appcalappliance}/>
                <Route path="/components/switches" name="Swithces" component={Switches}/>
                <Route path="/components/tables" name="Tables" component={Tables}/>
                <Route path="/components/tabs" name="Tabs" component={Tabs}/>
                <Route path="/icons/font-awesome" name="Font Awesome" component={FontAwesome}/>
                <Route path="/icons/simple-line-icons" name="Simple Line Icons" component={SimpleLineIcons}/>
                <Route path="/widgets" name="Widgets" component={Widgets}/>
                <Route path="/charts" name="Charts" component={Charts}/>
                <Redirect from="/" to="/login"/>
              </Switch>
            </Container>
          </main>
        </div>        
      </div>
    );
  }
}

export default Full;
